from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status
from datetime import datetime


@api_view(['GET'])
def index(request):
    date = datetime.now().strftime ("%d/%m/%Y %H:%M:%S")
    message = 'Clock in server is live time'
    objects = models.Manager()

    return Response(data=message + date)
